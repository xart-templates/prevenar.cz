jQuery.noConflict();
jQuery(document).ready(function($){



	$('html').addClass('js');


	// checkbox, radio style
	$('[type="checkbox"],[type="radio"]').wrap('<span class="checkbox_wrap"/>').after('<span class="checkbox_btn"/>');

	// select box
	$('select').wrap('<span class="select_wrap"/>');

	$('.header_slideshow').each(function(){
		var i = $('.item',this).length;
		if(i>1){
			$(this).append('<a class="prev">&lt;</a><a class="next">&gt;</a><div class="pager"/>');
			$('.items',this).cycle({
				slides: '.item',
				speed: 900,
				timeout: 10000,
				pauseOnHover: true,
				swipe: true,
				prev: $('.prev',this),
				next: $('.next',this),
				pager: $('.pager',this),
			});
		}
	});

	$('.menu_main').before('<button class="toggle_menu">Menu</button>');
	$('.toggle_menu').on('click',function(){
		$('body').toggleClass('menu-on');
	});


	// file input style
	$('input[type=file]').each(function(){
		var uploadText = $(this).data('value');
		if (uploadText) {
			var uploadText = uploadText;
		} else {
			var uploadText = 'Choose file';
		}
		var inputClass=$(this).attr('class');
		$(this).wrap('<span class="fileinputs"></span>');
		$(this)
			.parents('.fileinputs')
			.append($('<span class="fakefile"/>')
			.append($('<input class="input-file-text" type="text" />')
			.attr({'id':$(this)
			.attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'}))
			.append('<input type="button" value="'+uploadText+'">'));
		$(this)
			.addClass('type-file')
			.css('opacity',0);
		$(this)
			.bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake')
			.val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))
		})
	});



	// placeholder
	if (document.createElement('input').placeholder==undefined){
		$('[placeholder]').focus(function(){
			var input=$(this);
			if(input.val()==input.attr('placeholder')){
				input.val('');
				input.removeClass('placeholder')
			}
		}).blur(function(){
			var input=$(this);
			if(input.val()==''||input.val()==input.attr('placeholder')){
				input.addClass('placeholder');
				input.val(input.attr('placeholder'))
			}
		}).blur()
	}

});